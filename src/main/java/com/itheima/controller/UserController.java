package com.itheima.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.xml.ws.RequestWrapper;

/*
* 处理特有的请求业务
* */
@Controller
@RequestMapping("/user")  // 一级路径
public class UserController {

    /*
     * 返回值表示告诉spring要跳转的页面
     * */
    @RequestMapping("save")   // 二级路径  能访问：HTTP：localhost：8080/springmvc_01/user/save
    public String save(){
        System.out.println("save方法被访问了.......");
        return  "/page.jsp";
    }

    /*
    * 返回值表示告诉spring要跳转的页面
    * */

    @RequestMapping("/update")   // 二级路径  能访问：HTTP：localhost：8080/springmvc_01/user/update
    public String update(){
        System.out.println("update方法被访问了....");
        return "/page2.jsp" ;
    }

}
